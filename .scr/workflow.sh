#!/usr/bin/env bash

for param; do
  case $param in
  -v) set -x ;;
  -i) INTERACTIVE=tue ;;
  esac
done

set -e

mute () { "$@" &> /dev/null ; }
fs::pushd () { mute pushd "$1" || exit 1; }
fs::popd () { mute popd || exit 1; }
log::info () {
  if [[ -z "$INTERACTIVE" ]]; then echo "$1" ; else read -r -p "$(echo -e "\n$1\n[Press Enter to continue]")" DISCARD ; fi
}
log::graph () { echo "===" ; git::checkout develop ; GIT_PAGER="cat" git log --all --graph --oneline "$1"; echo "===" ; }

git::initOn () { mute git init ; mute git checkout -b "$1" ; }
git::currentBranch () { git rev-parse --abbrev-ref HEAD ; }
git::checkout  () { mute git checkout "$1" ; }
git::branch () { mute git checkout -b "$1" ; }
git::branchOn () { mute git checkout "$1" ; mute git checkout -b "$1" ; }
git::newBranch () { local CURRENT="$(git::currentBranch)" ; git::branch "$1" ; git::checkout "$CURRENT" ; }
git::branchPointsTo () { if [[ "$(git::currentBranch)" == "$1" ]]; then mute git reset --hard $2; else mute git branch -f "$1" "$2"; fi }
git::banchDelete () { mute git branch -D "$1" ; }
git::tag () { mute git tag "$1"; }
git::commit () { echo "$RANDOM" >> "$RANDOM" ; mute git add . ; mute git commit --allow-empty-message -m "" ; }
git::hash () { git log -1 --pretty=format:%h ; }
git::commitHash () { git::commit ; git::hash ; }
git::createFeatureBranch () { git::checkout develop ; git::branch "$1" ; git::commit ; git::commit ; git::checkout develop ; }
git::rebaseOn () { git::checkout "$1" ; mute git rebase $2 ; }
git::squashTo() { git::checkout "$1" ; mute git reset --soft "$2" ; mute git add . ; mute git commit --allow-empty-message -m "" ; }

REPO=$(mktemp -d)
fs::pushd "${REPO}"

git::initOn develop
git::commit
git::commit

git::newBranch prod
git::tag "prod_2021-10-01"
git::commit
git::commit
git::newBranch ivv
git::commit
git::commit

git::createFeatureBranch feature/A
log::graph develop

git::createFeatureBranch feature/B
log::graph develop

git::branchPointsTo develop feature/A
git::banchDelete feature/A
log::graph develop
log::info "Feature A is merged in develop"

git::rebaseOn "feature/B" develop
log::graph develop
log::info "Feature B is rebased on develop"


git::squashTo "feature/B" develop
log::graph develop
log::info "Feature B is squashed on develop"

fs::popd
rm -rf "${REPO}"
