# shellcheck shell=bash

#help: This examples demonstrates the use of positional parameters and options.

#arg: src  | file        | ARG1 | The 1st positional argument 
#arg: prop | aaa:bbb:ccc | ARG2 | The 2nd positional argument
#arg: dest | dir*        | ARG3 | The 3rd positional argument, repeatable

#opt: -b | flag                 | FLAG_OPT  | Some flag
#opt: -c | cx:cy:cz             | COLON_OPT | Completion with ':' separated values
#opt: -d | dir*                 | DIR_OPT   | Some dir, repeatable
#opt: -e | $(printf "e1 e2 e3") | EVAL_OPT  | Completion with arbitrary command
#opt: -f | file                 | FILE_OPT  | Some file

echo "ARG1=$ARG1"
echo "ARG2=$ARG2"
echo "ARG3=(" "${ARG3[@]}" ")"
echo "FLAG_OPT=$FLAG_OPT"
echo "COLON_OPT=$COLON_OPT"
echo "EVAL_OPT=$EVAL_OPT"
echo "FILE_OPT=$FILE_OPT"
echo "DIR_OPT=(" "${DIR_OPT[@]}" ")"
