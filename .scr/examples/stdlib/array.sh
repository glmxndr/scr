array::new SOME_ARR "a" "b"

declare -p SOME_ARR

array::from_lines SOME_OTHER_ARR 'a\nb\nc'

declare -p SOME_OTHER_ARR

array::append SOME_OTHER_ARR "d"

declare -p SOME_OTHER_ARR

array::remove_value SOME_OTHER_ARR "b"

declare -p SOME_OTHER_ARR