# shellcheck shell=bash

[[ -e "$SCR_EXEC_BCKP" ]] && rm "$SCR_EXEC_BCKP"
[[ -e "$SCR_EXEC" ]] && mv "$SCR_EXEC" "$SCR_EXEC_BCKP"
ln -s "$(realpath "$PROJECT_HOME/scr")" "$SCR_EXEC"
