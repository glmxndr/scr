# shellcheck shell=bash

SCR_EXEC="$HOME/.local/bin/scr"
if [[ -e "$SCR_EXEC_BCKP" ]]; then
  if [[ -e "$SCR_EXEC" ]]; then
    rm "$SCR_EXEC"
  fi
  mv "$SCR_EXEC_BCKP" "$SCR_EXEC"
fi