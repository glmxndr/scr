# shellcheck shell=bash disable=SC2086

ARGOPT_CMD="scr --complete examples argopt"

test::cmd "1nd positional argument (type=file)" \
${ARGOPT_CMD} -- R \
<< EOEXPECTED
README.txt
EOEXPECTED

test::cmd "2nd positional argument (type=aaa:bbb:ccc)" \
${ARGOPT_CMD} -- README.txt "" \
<< EOEXPECTED
aaa
bbb
ccc
EOEXPECTED

test::cmd_sort "3nd positional argument (type=dir*)" \
${ARGOPT_CMD} -- README.txt aaa ".scr/" \
<< EOEXPECTED
.scr/link
.scr/test
.scr/examples
EOEXPECTED

test::cmd_sort "-c option (type=cx:cy:cz)" \
${ARGOPT_CMD} README.txt -d .scr -c "" \
<< EOEXPECTED
cx
cy
cz
EOEXPECTED

test::cmd_sort "-d option 1 (type=dir*)" \
${ARGOPT_CMD} README.txt -d ".s" \
<< EOEXPECTED
.scr
EOEXPECTED

test::cmd_sort "-d option 2 (type=dir*)" \
${ARGOPT_CMD} README.txt -d .scr -d ".s" \
<< EOEXPECTED
.scr
EOEXPECTED

test::cmd_sort "-e option (type=\$(echo e1 e2 e3))" \
${ARGOPT_CMD} README.txt -d .scr -e "e" \
<< EOEXPECTED
e1
e2
e3
EOEXPECTED

test::cmd_sort "-f option (type=file)" \
${ARGOPT_CMD} README.txt -f "R" \
<< EOEXPECTED
README.txt
EOEXPECTED

test::cmd "Complete parse" \
scr examples argopt -b -c cx -d od1 -d od2 -e e1 -f of -- pf aaa pd1 pd2 \
<< EOEXPECTED
ARG1=pf
ARG2=aaa
ARG3=( pd1 pd2 )
FLAG_OPT=1
COLON_OPT=cx
EVAL_OPT=e1
FILE_OPT=of
DIR_OPT=( od1 od2 )
EOEXPECTED
