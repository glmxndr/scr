# shellcheck shell=bash

echo "Current script: $CURRSCRIPT"

echo -e "Befores: 
$(scr::locate_befores "$CURRSCRIPT")
"

echo -e "After: 
$(scr::locate_afters "$CURRSCRIPT")
"
