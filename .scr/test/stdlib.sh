
test::equal "cmd::arg_or_stdin by arg"  "abc" "$(cmd::arg_or_stdin "abc")"
test::equal "cmd::arg_or_stdin by pipe" "abc" "$(echo "abc" | cmd::arg_or_stdin)" 

test::equal "str::trim by arg"  "abc" "$(str::trim " abc ")"
test::equal "str::trim by pipe" "abc" "$(echo "  abc  " | str::trim)"

test::equal "str::column by arg"  "ghi" "$(str::column "|" 3 "abc | def | ghi | jkl")"
test::equal "str::column by pipe" "ghi" "$(echo "abc | def | ghi | jkl" | str::column "|" 3)"

LINES=$'first\nsecond\nthird'
test::equal "str::line by arg"  "second" "$(str::line 2 "$LINES")"
test::equal "str::line by pipe" "second" "$(echo -e "$LINES" | str::line 2)"
