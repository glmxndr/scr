
NAME
  scr - A SCRipt SubCommand Runner

SYNOPSIS
  scr --license           : shows the unlicense for scr
  scr --install           : install the base script
  scr --completions       : display completions script
  scr --list a b c        : list all subcommands under the subcommand 'a b c'
  scr --path a b c        : shows the path to the subcommand 'a b c'
  scr --new a b c         : initiate a script for subcommand 'a b c'
  scr --edit a b c        : edit the script for subcommand 'a b c' with $EDITOR
  scr --show a b c        : show the script for subcommand 'a b c' with 'cat'
  scr --help              : show this message
  scr --help a b c        : show help for subcommand 'a b c'
  scr --log [delete|show] : delete or display the log file
  scr a b c               : run subcommand 'a b c'
  scr                     : show help, or install if scr is sourced

DESCRIPTION
  Projects often require bash scripts to run chores, help with the setup,
  build or deploy artifacts. There are many ways to organize these scripts:
  some people create a single file with tons of subcommands in them, some
  others prefer to write as many scripts as there are commands to be run.
  Monolith scripts allow to share easily functions between subcommands, but
  requires work to have nice completion.
    On the other hand, having many scripts helps with command-line
  tab-completions, but may require more work to share code between them.

  The scr tools allows to define bash subcommands as a file tree
  under a base folder, and provides tab-completion. Sharing code between
  commands is done by putting this code in a '_init.sh' file at the correct
  level in the file tree.

  When invoking a user-defined subcommand, for instance:
    ~/project $ scr build local module
  the following things happen:
    • scr tries to find a '.scr' folder in the $PWD directory,
      or in the parent directories of $PWD (the '.scr' name can be changed
      by setting the DOT_SCR_NAME env var), and fails if such a directory is
      not found
    • scr sources the '.scr/.env' file
    • scr sources the '.scr/_init.sh' file
    • scr sources the '.scr/build/_init.sh' file
    • scr sources the '.scr/build/local/_init.sh' file
    • scr sources the '.scr/build/local/module.sh' file

INSTALL
  Use the following command:
    $ bash -c "$(curl -LsS https://gitlab.com/glmxndr/scr/-/raw/main/scr)"

COMPLETION
  When completions are activated, the user may be anywhere under the project
  directory containing '.scr', and tab-completion will work. If the project is
  in '~/project', and the '.scr' folder is in ~/project/.scr, the user can do
  the following:
    ~/project/module/submodule $ scr bu<tab>
    ~/project/module/submodule $ scr build <tab>
                                            local remote
    ~/project/module/submodule $ scr build local
    ~/project/module/submodule $ scr build local m<tab>
    ~/project/module/submodule $ scr build local module
  User-defined completion for user-defined commands is also possible, see the
  dedicated section below.

SPECIAL COMMANDS
  --license
    Display the license for this project.

  --help | -h
    This base command allows to display the help for the given command. For instance:
      ~/project $ scr --help build local module
    will parse the '~/project/.scr/build/local/module.sh' file and extract:
      • the synopsis from lines starting with ''#synopsis:', and
      • the description from lines starting with ''#help:'.

  --install | -i
    The scr.sh script can install itself in $HOME/.local/bin/scr
      ~/project $ ./scr.sh -i
    This makes SCR available as an executable program:
      ~/project $ scr ...

  --completions | -c
    The scr.sh script can be invoked to display the program to auto-complete
    subcommands, at least in bash and zsh.
      ~/project $ ./scr.sh -c

  --list | -l
    This base command allows to list all defined user commands.
      ~/project $ scr --list

  --new | -n
    This base command allows to create a new subcommand.
    For instance:
      ~/project $ scr --new build local module
    will initiate a file '~/project/.scr/build/local/module.sh'.
    If the EDITOR env var is set, this editor is used to edit the file.

  --edit | -e
    This base command allows to edit an existing subcommand.
    For instance:
      ~/project $ scr --edit build local module
    will launch '$EDITOR ~/project/.scr/build/local/module.sh'.

  --show | -s
    This base command allows to display the content of a subcommand script.
    For instance:
      ~/project $ scr --show build local module
    will launch 'cat ~/project/.scr/build/local/module.sh'
    (or bat if it is available).

  --log [show|delete]
    This base command allows to display or delete the log file. The default log
    file is in ~/.scr/debug.log, but can be changed with the SCR_DEBUG_FILE
    env variable. Debug logging must be enabled by setting the SCR_DEBUG env
    variable. Use log::debug in your subcommands to write to this file.

USER DEFINED COMMANDS
  All '.sh' files under the '.scr' and its recursive subdirectories are
  considered as eligible subcommands, but the 'new', 'list', and 'help' names
  are reserved and can not be at the first level.

  scr provides a mechanism to declare arguments and options for
  subcommands. This mechanism uses comments in the subcommand file.

  #arg: name | type | VARNAME | Description
  Adding this comment to the subcommand file declares that it accepts a
  positional argument, the value of which is available in the VARNAME
  variable.
  The 'name' and 'Description' are used for help generation.
  The 'type' is used for completion.

  #opt: -x | type | VARNAME | Description
  Adding this comment to the subcommand file declares that it accepts a '-x'
  argument, the value of which is available in the VARNAME variable. When an
  option is declared with type 'flag', no value must be given, and its value
  is set to '1' if found in the subcommand call.
  The 'Description' is used for help generation.
  The 'type' is used for completion.

USER DEFINED COMPLETIONS
  Creating a '<command>.complete.sh' file along with the use-defined command
  '<command>.sh' allows to define completion for a given command.
  For instance, if the following files exist:
      '~/project/.scr/build/local/module.sh'
      '~/project/.scr/build/local/module.complete.sh'
  then when completing past the module word, the module.complete.sh is called.
  For instance, when completing the following line:
    ~/project $ scr build local module xx yy<tab>
  the script module.complete.sh is called with arguments 'xx yy'.
  The script is supposed to write in stdout all the possible completions for
  the last word.
  The script is also given two environment variables:
      • DOT_SCR: the path to the '.scr' folder
      • PROJECT_HOME: the path to the parent of the '.scr' folder

INIT FILES
  Each recursive subdirectory can have a _init.sh file. All existing _init.sh
  files are sourced, from top (closest to .scr dir) to bottom (closest to
  the subcommand .sh file).

